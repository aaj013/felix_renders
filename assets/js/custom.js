$(document).ready(function(){
  $('.search-area').on('click',function(){
    $('.search-icon').hide();
    $('#search').show().focus();
  });

  $('#search').on('keypress',function(e){
    if(e.keyCode == 13){
      $(this).val('').hide();
      $('.search-icon').show();
    }
  });

  $('#search').on('focusout blur',function(e){
      $(this).val('').hide();
      $('.search-icon').show();
  });

  // Instantiate the Bootstrap carousel
  $('.multi-item-carousel').carousel({
    interval: false });


  // for every slide in carousel, copy the next slide's item in the slide.
  // Do the same for the next, next item.
  $('.multi-item-carousel .item').each(function () {
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    if (next.next().length > 0) {
      next.next().children(':first-child').clone().appendTo($(this));
    } else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    }
  });


});
